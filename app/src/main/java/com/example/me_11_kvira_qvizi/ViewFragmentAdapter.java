package com.example.me_11_kvira_qvizi;

import androidx.annotation.NonNull;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentActivity;
import androidx.viewpager2.adapter.FragmentStateAdapter;

import com.example.me_11_kvira_qvizi.fragments.FragmentFirst;
import com.example.me_11_kvira_qvizi.fragments.FragmentSecond;
import com.example.me_11_kvira_qvizi.fragments.FragmentThird;

public class ViewFragmentAdapter extends FragmentStateAdapter {
    public ViewFragmentAdapter(@NonNull FragmentActivity fragmentActivity) {
        super(fragmentActivity);
    }

    @NonNull
    @Override
    public Fragment createFragment(int position) {
        switch(position) {
            case 0:
                return new FragmentFirst();
            case 1:
                return new FragmentSecond();
            case 2:
                return new FragmentThird();
            default:
                return null;
        }
    }

    @Override
    public int getItemCount() {
        return 3;
    }
}
