package com.example.me_11_kvira_qvizi.fragments;

import android.os.Bundle;
import android.view.View;
import android.widget.ImageView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;

import com.example.me_11_kvira_qvizi.R;

public class FragmentThird extends Fragment {
    private ImageView imageView3;
    public FragmentThird(){
        super(R.layout.fragment_third);
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        imageView3 = view.findViewById(R.id.imageView3);
    }
}
