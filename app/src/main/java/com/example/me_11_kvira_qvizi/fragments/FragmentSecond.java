package com.example.me_11_kvira_qvizi.fragments;

import android.os.Bundle;
import android.view.View;
import android.widget.ImageView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;

import com.example.me_11_kvira_qvizi.R;

public class FragmentSecond extends Fragment {
    private ImageView imageView2;


    public FragmentSecond(){
        super(R.layout.fragment_second);
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        imageView2 = view.findViewById(R.id.imageView2);
    }
}
